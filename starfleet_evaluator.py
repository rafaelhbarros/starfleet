import string

COMMANDS = {
    'alpha': [(-1, -1), (-1, 1), (1, -1), (1, 1)],
    'beta': [(-1, 0), (0, -1), (0, 1), (1, 0)],
    'gamma': [(-1, 0), (0, 0), (1, 0)],
    'delta': [(0, -1), (0, 0), (0, 1)]
}

MOVES = {
    'north': (0, 1),
    'south': (0, -1),
    'east': (1, 0),
    'west': (-1, 0)
}

DISTANCES = {l: i+1 for i, l in enumerate(string.letters)}
letter_set = set(string.letters)

def read_from_file(filename):
    with open(filename) as f:
        for l in f.readlines():
            yield l.strip()

def read_grid(filename):
    grid = []
    mines = 0
    for line in read_from_file(filename):
        row = []
        for i in line:
            if i in letter_set:
                mines += 1
            row.append(i)
        grid.append(row)
    return grid, mines
    # return [list(i) for i in read_from_file(filename)]

def read_commands(filename):
    return [i.split() for i in read_from_file(filename)]

def compute_fall_map(grid, position):
    new_map = []
    passed_mines = 0
    colision = False
    for i, row in enumerate(grid):
        new_row = []
        for j, v in enumerate(row):
            v = DISTANCES.get(v, v)
            if not isinstance(v, int):
                new_row.append(v)
                continue
            v -= 1
            if v == 0:
                colision = colision or (i, j) == position
                passed_mines += 1
                v = '*'
            else:
                v = string.letters[v-1]
            new_row.append(v)
        new_map.append(new_row)
    return new_map, colision, passed_mines

def _trim_grid(grid):
    while grid[0] == grid[-1] and all(i == '.' for i in grid[0]) and len(grid) >= 3:
        grid = grid[1:-1]
    if len(grid[0]) == 1: # don't want to trim too much, same as len >= 3 above
        return grid
    exit = False
    while not exit:
        ngrid = []
        for row in grid:
            f = row[0]
            l = row[-1]
            if not (f == l == '.'):
                exit = True
                break
            ngrid.append(row[1:-1])
        if not exit:
            grid = ngrid
    return grid

def move(grid, direction, position):
    # inneficient, but works
    width = len(grid[0])
    if direction == 'north':
        position[1] += 1
        grid.insert(0, ['.']*width)
        if len(grid) % 2.0 == 0:
            grid.insert(0, ['.']*width)
    elif direction == 'south':
        position[1] -= 1
        grid.append(['.']*width)
        if len(grid) % 2.0 == 0:
            grid.append(['.']*width)
    elif direction == 'east':
        position[0] += 1
        for row in grid:
            row.append('.')
            if len(row) % 2.0 == 0:
                row.append('.')
    elif direction == 'west':
        position[0] -= 1
        for row in grid:
            row.insert(0, '.')
            if len(row) % 2.0 == 0:
                row.insert(0, '.')

    return grid, position

def fire(grid, pattern, position):
    x, y = position
    coords = [(x+i, y+j) for i, j in COMMANDS[pattern]]
    hits = 0
    for x, y in coords:
        try:
            point = grid[x][y]
        except IndexError:
            continue
        if point in letter_set:
            hits += 1
            grid[x][y] = '.'
            break # no splash damage, only one hit
    return grid, hits

def print_grid_string(grid):
    for row in grid:
        print(''.join(row))

def calculate_score(mines, destroyed, shots, moves, steps, taken, colision):
    if destroyed < mines or colision:
        return 'fail', 0
    if mines == destroyed and taken < steps:
        return 'pass', 1
    score = (mines * 10) - (min(moves*2, mines*3)) - (min(shots*5, mines*5))
    return 'pass', score


def main(map_file, command_file):
    grid, mines = read_grid(map_file)
    grid = _trim_grid(grid)
    commands = read_commands(command_file)
    position = [len(grid) / 2, len(grid[0]) / 2]
    destroyed = 0
    shots = 0
    moves = 0

    steps = len(commands)

    for i, command in enumerate(commands):
        print "\nStep: {}".format(i+1), '\n'
        print_grid_string(grid)
        for piece in command:
            if piece in MOVES:
                moves += 1
                grid, position = move(grid, piece, position)
            elif piece in COMMANDS:
                shots += 1
                grid, hits = fire(grid, piece, position)
                destroyed += hits
        print '\n', ' '.join(command), '\n'
        grid = _trim_grid(grid)
        grid, colision, missed = compute_fall_map(grid, position)
        print_grid_string(grid)
        if colision or missed or destroyed == mines:
            break

    success, score = calculate_score(
        mines, destroyed, shots, moves, steps, i+1, colision)

    print '{} ({})'.format(success, score)


if __name__ == '__main__':
    import sys
    main(sys.argv[1], sys.argv[2])
